<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function home()
    {
        return view('homepage');
    }

    public function index()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'=> 'required',
            'password' => 'required'
        ]);

        // if($request->has('remember')){
        //     Cookie::queue(Cookie::make('adminemail', $request->email,1440));
        //     Cookie::queue(Cookie::make('adminpassword', $request->password,1440));
        // }

        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)) {
            return redirect()->intended('dashboard')
                        ->with('message', 'signed in!');
        }

        return redirect('/login')->with('message', 'login details are not valid!');

    }

    public function signup()
    {
        return view('registration');
    }

    public function signupsave(Request $request ){

        $request->validate([
            'name' => 'required',
            'email'=> 'required|email|unique:users',
            'password' => 'required|min:6'
        ]);

        $data = $request->all();
        $check = $this->create($data);

        return redirect("dashboard");
    }

    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email'=>  $data['email'],
            'password' =>  Hash::make($data['password'])
        ]);
    }

    public function dashboard()
    {
       if(Auth::check()){
            return view('dashboard');
       }
       return redirect('/login');
    }

    public function signOut()
    {
        Session::flush();
        // dd(Cookie::forget('adminemail'));
        // dd(Cookie::forget('adminpassword'));
        Auth::logout();

       return redirect('login');
    }

}
